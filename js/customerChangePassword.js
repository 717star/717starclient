$("#frmChangePassword").on("submit", function () {
    var actualPassword = $("#actual").val();
    var newPassword = $("#new").val();
    var repeatPassword = $("#repeat").val();
    if (newPassword == repeatPassword) {
        var jsonReq = {
            email: $.cookie("email"),
            actualPassword: actualPassword,
            newPassword: newPassword
        };
        var rol = $.cookie("rol");
        if (rol == "buyer") {
            $.ajax({
                type: 'GET',
                contentType: 'application/json',
                url: apiServer + "buyer/updatePassword",
                data: jsonReq,
                crossDomain: true,
                dataType: 'jsonp',
                success: function (jsonRes) {
                    if(jsonRes.status == 200) {
                        $(".botton-messages").hide();
                        window.location.href = "index.html";
                    } else {
                        $(".botton-messages").show();
                        $(".botton-messages").html(jsonRes.msg);
                    }
               }
            });
        }
        else {
            $.ajax({
                type: 'GET',
                contentType: 'application/json',
                url: apiServer + "seller/updatePassword",
                data: jsonReq,
                crossDomain: true,
                dataType: 'jsonp',
                success: function (jsonRes) {
                    if(jsonRes.status == 200) {
                        $(".botton-messages").hide();
                        window.location.href = "index.html";
                    } else {
                        $(".botton-messages").show();
                        $(".botton-messages").html(jsonRes.msg);
                    }
                }
            });
        }
    }
    else {
        $(".botton-messages").show();
        $(".botton-messages").html("PASSWORDS NOT MATCH!");
    }
    return false;
});