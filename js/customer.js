// RECOVER PASSWORD
function recoverPassword(){
    $(".botton-messages").hide();
    $("#centerMain").load("customerRecoverPassword.html");
}

// SIGN UP
function signUp(){
    $(".botton-messages").hide();
    $("#centerMain").load("customerSignUp.html");
}

function signUpBuyer(){
    $(".botton-messages").hide();
    $("#centerMain").load("buyerSignUp.html");
}

function signUpSeller(){
    $(".botton-messages").hide();
    $("#centerMain").load("sellerSignUp.html");
}

function irAIndex(){
    window.location.href = "index.html";
}

$(document).ready(function () {
    $("#centerMain").load("customerLogon.html");
});

