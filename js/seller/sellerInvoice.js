// Cells with states
function pintarEstados(value){
    var classes = ["state-red", "state-green", "state-blue", "state-gray"];
    if(value.indexOf("CANCEL") !== -1)
    {
        return { classes: "state-red"}
    }
    if(value.indexOf("IN BIDDING")!== -1 || value.indexOf("Sold")!== -1)
    {
        return {classes: "state-green"}
    }
    if(value.indexOf("PENDING VALIDATION")!== -1)
    {
        return {classes: "state-gray"}
    }
    if(value.indexOf("NO SOLD")!== -1)
    {
        return {classes: "state-light-blue"}
    }
    return {classes: "state-blue"}
}

function edit(){
    $("#modal").load("views/seller/sellerInvoiceEdit.html");
}
function cancel(){
    $("#modal").load("views/seller/sellerInvoiceCancel.html");
}
function confirm(){
    $("#modal").load("views/seller/sellerInvoiceMakeDeal.html");
}
function republish(){
    $("#modal").load("views/seller/sellerInvoiceRepublish.html");
}

$(document).ready(function () {
    $("#newInvoice").click(function(){
        $("#centerMain").load("views/seller/sellerInvoiceNew.html");
    });
    var jsonReq = {
        "seller": $.cookie("userId")
    };    
    $.ajax({
        type: 'GET', 
        contentType: 'application/json', 
        url: apiServer + "invoice/readAllToDeal", 
        data: jsonReq, 
        crossDomain: true, 
        dataType: 'jsonp',
        success: function(jsonRes) {
            if (jsonRes.status == 200) {
                var data = [];
                for (var i = 0, l = jsonRes.obj.length; i < l; i++) {
                    var jsonItem = jsonRes.obj[i];
                    var item = {};
                    item.id = jsonItem._id; // TODO: real data not NA
                    item.invoiseId = jsonItem.invoiseId;
                    item.buyer = "NA";
                    item.purchaseDate = "NA";
                    item.issueDate = jsonItem.issueDate;
                    item.daysMaturity = "NA";
                    item.totalAmount = jsonItem.totalAmount;
                    item.askAdvance = jsonItem.askAdvance;
                    item.askDiscountRate = jsonItem.askDiscountRate;
                    item.biddingClosing = "NA";
                    item.status = jsonItem.status;
                    item.bestAdvance = "NA";
                    item.bestDiscount = "NA";
                    item.edit = "<i class=\"icono-borde glyphicon glyphicon-edit color-dark-blue\" onclick='edit();'></i>";
                    item.remove = "<i class=\"icono-borde glyphicon glyphicon-remove color-red\" onclick='cancel();'></i>";
                    item.ok = "<i class=\"icono-borde glyphicon glyphicon-ok color-green\" onclick='confirm();'></i>";
                    item.repeat = "<i class=\"icono-borde glyphicon glyphicon-repeat color-dark-blue\" onclick='republish();'></i>"
                    data.push(item);
                }
                $('#profile-table').bootstrapTable({
                    data: data
                });
                $('#profile-table').bootstrapTable('hideLoading');
                // Color encabezado
                $("#profile-table thead tr:last-child th").addClass("header-profile-table");
                // Tooltip
                $('[data-toggle="popover"]').popover();
            } else {
                $(".botton-messages").show();
                $(".botton-messages").html(jsonRes.msg);
            }
        }
    });
    $('#profile-table').on('click-cell.bs.table', function(e, value, row, element){
        $.cookie("invoiceId", element.id);
    });
});