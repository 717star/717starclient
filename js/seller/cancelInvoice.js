$(document).ready(function () {
    $("#confirmInvoice").click(function(){
        $("#centerMain").load("views/seller/sellerInvoiceCancelConfirm.html");
    });
    $("#cancelInvoice").click(function(){
        $("#centerMain").load("views/seller/sellerInvoice.html");
    });
    var jsonReq = {
        "_id": $.cookie("invoiceId")
    };
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: apiServer + "invoice/readById",
        data: jsonReq,
        crossDomain: true,
        dataType: 'jsonp',
        success: function(jsonRes) {
            if (jsonRes.status == 200) {
                var data = [];
                var jsonItem = jsonRes.obj;
                var item = {};
                item.invoiseId = jsonItem.invoiseId;
                item.payer = jsonItem.payerImporter.companyName;
                item.issueDate = jsonItem.issueDate;
                item.daysMaturity = "NA";
                item.totalAmount = jsonItem.totalAmount;
                item.askAdvance = jsonItem.askAdvance;
                item.askDiscountRate = jsonItem.askDiscountRate;
                item.status = jsonItem.status;
                item.bestAdvance = "NA";
                item.bestDiscount = "NA";
                data.push(item);
                $('#cancel-table').bootstrapTable({
                    data: data
                });
                $('#cancel-table').bootstrapTable('hideLoading');
                // Color encabezado
                $("#cancel-table thead tr:last-child th").addClass("header-profile-table");
            } else {
                $(".botton-messages").show();
                $(".botton-messages").html(jsonRes.msg);
            }
        }
    });
    $("#modalCancel").modal("show");
});