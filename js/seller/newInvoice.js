$(document).ready(function () {
    // TODO: pdf uploader
/*    $("#btnUploadPDF").click(function(){
        $("#btnUploadPDF").val("nameofdoc.pdf");
        $("#btnOkAndRemoveUploadPDF").show();
    });*/

    //TODO: send invoice form to confirm
/*    $("#confirmInvoice").click(function(){
        $("#centerMain").load("views/seller/sellerInvoiceNewConfirm.html");
    });*/

    $("#frmNewInvoive").on("submit", function(){
        var jsonReq = {
            payerImporter: {
                companyName: $("#companyName").val(),
                country: $("#country").val(),
                contact: {
                    person: $("#person").val(),
                    email: $("#email").val(),
                    phone: $("#phone").val(),
                    cel: $("#cel").val(),
                }
            },
            invoiseId: $("#invoiseId").val(),
            totalAmount: $("#totalAmount").val(),
            issueDate: $("#issueDate").val(),
            expirationDate: $("#expirationDate").val(),
            sent: $("#sentYes").val(),
            billOfLadingDate: $("#billOfLadingDate").val(),
            incoterms: $("#incoterms").val(),
            authorizedByDebtor: $("#authorizedByDebtorYes").val(),
            askAdvance: $("#askAdvance").val(),
            askDiscountRate: $("#askDiscountRate").val(),
            seller: $.cookie("userId")
        };
        $.ajax({
            type: 'GET', 
            contentType: 'application/json', 
            url: apiServer + "invoice/create", 
            data: jsonReq, 
            crossDomain: true, 
            dataType: 'jsonp',
            success: function(jsonRes) {
                if (jsonRes.status == 200) {
                } else {
                    $(".botton-messages").show();
                    $(".botton-messages").html(jsonRes.msg);
                }
                $("#centerMain").load("views/seller/sellerInvoice.html");
            }
        });
        return false;
    });
});