$(document).ready(function () {
    var jsonReq = {
        "_id": $.cookie("invoiceId")
    };
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: apiServer + "invoice/readById",
        data: jsonReq,
        crossDomain: true,
        dataType: 'jsonp',
        success: function(jsonRes) {
            if (jsonRes.status == 200) {
                $("#askAdvance").val(jsonRes.obj.askAdvance);
                $("#askDiscountRate").val(jsonRes.obj.askDiscountRate);
            } else {
                $(".botton-messages").show();
                $(".botton-messages").html(jsonRes.msg);
            }
            $("#modalEdit").modal("show");
        }
    });
    $("#frmEditInvoive").on("submit", function(){
        var jsonReq = {
            "_id": $.cookie("invoiceId"),
            "askAdvance": $("#askAdvance").val(),
            "askDiscountRate": $("#askDiscountRate").val()
        };
        $.ajax({
            type: 'GET',
            contentType: 'application/json',
            url: apiServer + "invoice/updateAsk",
            data: jsonReq,
            crossDomain: true,
            dataType: 'jsonp',
            success: function(jsonRes) {
                if (jsonRes.status == 200) {
                } else {
                    $(".botton-messages").show();
                    $(".botton-messages").html(jsonRes.msg);
                }
                $("#centerMain").load("views/seller/sellerInvoice.html");
            }
        });
        return false;
    });
});