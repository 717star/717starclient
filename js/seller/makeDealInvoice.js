$(document).ready(function () {
    var jsonReq = {
        "_id": $.cookie("invoiceId")
    };
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: apiServer + "invoice/readById",
        data: jsonReq,
        crossDomain: true,
        dataType: 'jsonp',
        success: function(jsonRes) {
            if (jsonRes.status == 200) {
                $("#invoiseId").html(jsonRes.obj.invoiseId);
                $("#companyName").html(jsonRes.obj.payerImporter.companyName);
                $("#issueDate").html(jsonRes.obj.issueDate);
                $("#daysToMaturiry").html("N/A"); // TODO
                $("#totalAmount").html(jsonRes.obj.totalAmount);
                $("#askAdvance").html(jsonRes.obj.askAdvance);
                $("#askDiscountRate").html(jsonRes.obj.askDiscountRate);
                $("#status").html(jsonRes.obj.status);
                $("#bidAdvance").html("N/A"); // TODO
                $("#bidDiscountRate").html("N/A"); // TODO
            } else {
                $(".botton-messages").show();
                $(".botton-messages").html(jsonRes.msg);
            }
            $("#modalConfirm").modal("show");
        }
    });
    $("#confirmInvoice").click(function(){
        $("#divModalConfirm").load("views/seller/sellerInvoiceMakeDealConfirm.html");
    });
    $("#cancelInvoice").click(function(){
        $("#centerMain").load("views/seller/sellerInvoice.html");
    });
});