$(document).ready(function () {
    $("#confirmInvoice2").click(function(){
        var jsonReq = {
            "invoice": $.cookie("invoiceId")
        };
        $.ajax({
            type: 'GET',
            contentType: 'application/json',
            url: apiServer + "bid/readAllByInvoice",
            data: jsonReq,
            crossDomain: true,
            dataType: 'jsonp',
            success: function(jsonRes) {
                if (jsonRes.status == 200) {
                    jsonReq = {
                        "bid": jsonRes.obj[0]._id // TODO: bid calculation
                    };
                    $.ajax({
                        type: 'GET',
                        contentType: 'application/json',
                        url: apiServer + "transaction/create",
                        data: jsonReq,
                        crossDomain: true,
                        dataType: 'jsonp',
                        success: function(jsonRes) {
                            if (jsonRes.status == 200) {
                            } else {
                                $(".botton-messages").show();
                                $(".botton-messages").html(jsonRes.msg);
                            }
                            $("#centerMain").load("views/seller/sellerInvoice.html");
                        }
                    });
                } else {
                    $(".botton-messages").show();
                    $(".botton-messages").html(jsonRes.msg);
                }
            }
        });
    });
    $("#cancelInvoice2").click(function(){
        $("#centerMain").load("views/seller/sellerInvoice.html");
    });
    $("#modalDealConfirm").modal("show");
});