function bid(){
    // Balance
    var jsonReq = {
        "buyer": $.cookie("userId"),
        "status": "ACTIVE"
    };
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: apiServer + "buyerMovement/readAllByBuyerAndStatus",
        data: jsonReq,
        crossDomain: true,
        dataType: 'jsonp',
        success: function(jsonRes) {
            if (jsonRes.status == 200) {
                var balance = 0;
                for (var i = 0, l = jsonRes.obj.length; i < l; i++) {
                    balance += jsonRes.obj[i].movementType == "WITHDRAWAL"?  (-1)*jsonRes.obj[i].amount : jsonRes.obj[i].amount;
                }
                $("#accountNumberOffer").html("NA"); // TODO: account information on logon
                $("#ammountOffer").html(balance);
            } else {
                $(".botton-messages").show();
                $(".botton-messages").html(jsonRes.msg);
            }
        }
    });
    // Invoice info to offer
    var jsonReq = {
        "_id": $.cookie("invoiceId")
    };
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: apiServer + "invoice/readById",
        data: jsonReq,
        crossDomain: true,
        dataType: 'jsonp',
        success: function(jsonRes) {
            if (jsonRes.status == 200) { // TODO: NAs!!!
                $("#invoiceInfo").html(""+
                    "Seller: " + jsonRes.obj.seller.companyName + "<br/>"+
                    "Amount: " + jsonRes.obj.totalAmount + "<br/>"+
                    "Days of Maturity: " + "N/A" + "<br/>"+
                    "Bidding Closing: " + "N/A"
                );
            } else {
                $(".botton-messages").show();
                $(".botton-messages").html(jsonRes.msg);
            }
            $("#modalEdit").modal("show");
        }
    });
    $("#info-bid").show();
}

function confirmBid(){
    var jsonReq = {
        "buyer": $.cookie("userId"),
        "invoice": $.cookie("invoiceId"),
        "askAdvance": $("#advance").val(),
        "askDiscountRate": $("#discountRate").val()
    }
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: apiServer + "bid/create",
        data: jsonReq,
        crossDomain: true,
        dataType: 'jsonp',
        success: function(jsonRes) {
            if (jsonRes.status == 200) {
            } else {
                $(".botton-messages").show();
                $(".botton-messages").html(jsonRes.msg);
            }
            $("#info-bid").hide();
        }
    });
}

function closeBid(){
    $("#info-bid").hide();
}

$(document).ready(function () {
    // Balance
    var jsonReq = {
        "buyer": $.cookie("userId"),
        "status": "ACTIVE"
    }
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: apiServer + "buyerMovement/readAllByBuyerAndStatus",
        data: jsonReq,
        crossDomain: true,
        dataType: 'jsonp',
        success: function(jsonRes) {
            if (jsonRes.status == 200) {
                var balance = 0;
                for (var i = 0, l = jsonRes.obj.length; i < l; i++) {
                    balance += jsonRes.obj[i].movementType == "WITHDRAWAL"?  (-1)*jsonRes.obj[i].amount : jsonRes.obj[i].amount;
                }
                $("#accountNumber").html($("#accountNumber").html() + " " + "NA"); // TODO: account information on logon
                $("#ammount").html($("#ammount").html() + " " + balance);
            } else {
                $(".botton-messages").show();
                $(".botton-messages").html(jsonRes.msg);
            }
        }
    });
    // Invoices to offer
    var jsonReq = {
        "seller": $("#seller").val()
    };
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: apiServer + "invoice/readAllToBid",
        data: jsonReq,
        crossDomain: true,
        dataType: 'jsonp',
        success: function(jsonRes) {
            if (jsonRes.status == 200) {
                var data = [];
                for (var i = 0, l = jsonRes.obj.length; i < l; i++) {
                    var jsonItem = jsonRes.obj[i];
                    var item = {};
                    item.id = jsonItem._id; // TODO: real data not NA
                    item.seller = jsonItem.seller.companyName;
                    item.payer = jsonItem.payerImporter.companyName;
                    item.totalAmount = jsonItem.totalAmount;
                    item.daysMaturity = "NA";
                    item.biddingClosing = "NA";
                    item.bestAdvance = "NA";
                    item.bestDiscount = "NA";
                    item.actions = "<button class=\"btn-green\" onclick='bid();'>BID</button>";
                    data.push(item);
                }
                $('#offer-invoice').bootstrapTable({
                    data: data
                });
                $('#offer-invoice').bootstrapTable('hideLoading');
                // Color encabezado
                $("#offer-invoice thead tr th").addClass("bold header-profile-table");
            } else {
                $(".botton-messages").show();
                $(".botton-messages").html(jsonRes.msg);
            }
        }
    });
    $('#offer-invoice').on('click-cell.bs.table', function(e, value, row, element){
        $.cookie("invoiceId", element.id);
    });
});