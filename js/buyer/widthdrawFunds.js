var ammoun = 0;

$(document).ready(function () {
    var jsonReq = {
        "buyer": $.cookie("userId"),
        "status": "ACTIVE"
    }
    $.ajax({
        type: 'GET', 
        contentType: 'application/json', 
        url: apiServer + "buyerMovement/readAllByBuyerAndStatus", 
        data: jsonReq, 
        crossDomain: true, 
        dataType: 'jsonp',
        success: function(jsonRes) {
            if (jsonRes.status == 200) {
                var balance = 0;
                for (var i = 0, l = jsonRes.obj.length; i < l; i++) {
                    balance += jsonRes.obj[i].movementType == "WITHDRAWAL"?  (-1)*jsonRes.obj[i].amount : jsonRes.obj[i].amount;
                }
                $("#accountNumberBalance").html("NA"); // TODO: account information on logon
                $("#ammountBalance").html(balance);
                $("#accountNumberFunds").html("NA");
                $("#ammountFunds").html(balance);
                $("#routingNumberFunds").html("NA");
                $("#accountNumberFunds2").html("NA");
            } else {
                $(".botton-messages").show();
                $(".botton-messages").html(jsonRes.msg);
            }
        }
    });
    $("#widthdrawAccountBalance").click(function(){
        $("#centerMain").load("views/buyer/buyerWidthdrawFunds.html");
    });
    $("#widthdrawFunds").click(function(){ // TODO: ammount validation
        ammount = $("#ammount").val();
        $("#centerMain").load("views/buyer/buyerWidthdrawConfirm.html");
    });
    $("#confirmWidthdrawFunds").click(function(){
        var jsonReq = {
            buyer: $.cookie("userId"),
            amount: ammount,
            movementType: "WITHDRAWAL"
        };
        $.ajax({
            type: 'GET', 
            contentType: 'application/json', 
            url: apiServer + "buyerMovement/create", 
            data: jsonReq, 
            crossDomain: true, 
            dataType: 'jsonp',
            success: function(jsonRes) {
                if (jsonRes.status == 200) {
                    $("#centerMain").load("views/buyer/buyerAccountBalance.html");
                } else {
                    $(".botton-messages").show();
                    $(".botton-messages").html(jsonRes.msg);
                }
            }
        });
    });
    $("#cancelWidthdrawFunds").click(function(){
        $("#centerMain").load("views/buyer/buyerAccountBalance.html");
    });
    $("#modalWidthdraw").modal("show");
    $("#modalWidthdrawConfirm").modal("show");
});