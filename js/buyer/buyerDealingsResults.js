// Cells with states
function pintarEstados(value){
    var classes = ["state-red", "state-green", "state-blue"];
    if(value == "N/A" || value == "OVERDUE")
    {
        return { classes: "state-red"}
    }
    if(value == "PENDING OF CONFIRMATION" || value == "PAID TO THE SELLER")
    {
        return {classes: "state-green"}
    }
    return {classes: "state-blue"}
}

$(document).ready(function () {
    // Transactions
    var jsonReq = {
        "buyer": $.cookie("userId")
    };
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: apiServer + "transaction/readAllByBuyer",
        data: jsonReq,
        crossDomain: true,
        dataType: 'jsonp',
        success: function(jsonRes) {
            if (jsonRes.status == 200) {
                var data = [];
                for (var i = 0, l = jsonRes.obj.length; i < l; i++) {
                    var jsonItem = jsonRes.obj[i];
                    var item = {};
                    item.transactionClosingDate = "NA"; // TODO: real data not NA
                    item.seller = "NA";
                    item.totalAmount = "NA";
                    item.bestAdvance = "NA";
                    item.bestDiscount = "NA";
                    item.daysMaturity = "NA";
                    item.purchaseStatus = jsonItem.statusInvoiceBuying;
                    item.paymentStatus = jsonItem.statusInvoicePayment;
                    data.push(item);
                }
                $('#dealings').bootstrapTable({
                    data: data
                });
                $('#dealings').bootstrapTable('hideLoading');
                // Color encabezado
                $("#dealings thead tr th").addClass("bold header-profile-table");
            } else {
                $(".botton-messages").show();
                $(".botton-messages").html(jsonRes.msg);
            }
        }
    });
});
