// JavaScript Document

var blink = {
    
    COUNTER: 0,
    BLINKS: 0,
    SELECTOR: "",
    BACKGROUND: "",
    COLOR: "",
    BACKGROUND_FINAL: "",
    COLOR_FINAL: "",
    
    init: function(blinks, selector, background, color, backgroundFinal, colorFinal) {
        this.BLINKS = blinks;
        this.SELECTOR = selector;
        this.BACKGROUND = background;
        this.COLOR = color;
        this.COUNTER = 0;
        this.start();
    }, 
    
    start: function() {
        $(this.SELECTOR).css("color", this.BACKGROUND);
        $(this.SELECTOR).css("background-color", this.COLOR);
        setTimeout(function() {blink.next();},500);        
    },
    
    next: function() {
        $(this.SELECTOR).css("color", this.COLOR);
        $(this.SELECTOR).css("background-color", this.BACKGROUND);
        this.COUNTER++;
        //console.log("blink " + this.COUNTER);
        if (this.COUNTER <= this.BLINKS) {
            setTimeout(function() {blink.start();},500);
        } else { 
            $(this.SELECTOR).css("color", this.COLOR_FINAL);
            $(this.SELECTOR).css("background-color", this.BACKGROUND_FINAL);            
        }
    }
};

