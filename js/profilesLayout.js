function showSeller() {
    $("#sidebarInvoice").show();
    $("#sellerNewInvoice").show();
    $("#sidebarSellerProfile").show();
}
function showBuyer() {
    $("#sidebarAccountBalance").show();
    $("#sidebarOfferInvoice").show();
    $("#sidebarDealings").show();
    $("#sidebarBuyerProfile").show();
}
function showAdmin() {
    $("#adminBuyers").show();
    $("#adminSellers").show();
    $("#adminInvoices").show();
}
$(document).ready(function () {
    var rol = $.cookie("rol");
    if (rol == "seller") {
        showSeller();
    }
    else if (rol == "buyer") {
        showBuyer();
    }
    else { // Admin
        showAdmin();
    }
    $("#aLogout").on("click", function () {
        $.removeCookie('rol');
        $.cookie("authorized", false);
        window.location.href = 'customer.html';
        return false;
    });
});
