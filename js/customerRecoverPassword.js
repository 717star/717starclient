$("#frmRecoverPassword").on("submit", function () {
    var jsonReq = {
        email: $("#email").val()
    };
    $.ajax({
        type: 'GET', 
        contentType: 'application/json', 
        url: apiServer + "buyer/resetPassword",
        data: jsonReq, 
        crossDomain: true, 
        dataType: 'jsonp',
        success: function(jsonRes) {
            if(jsonRes.status == 200) {
                $.cookie("email", $("#email").val());
                $.cookie("rol", "buyer");
                $.ajax({
                    type: 'GET',
                    contentType: 'application/json',
                    url: apiServer + "email/"+jsonRes.obj.email+'/'+jsonRes.obj.password,
                    data: jsonReq,
                    crossDomain: true,
                    dataType: 'jsonp'
                });
                $("#centerMain").load("customerChangePassword.html");
                $(".botton-messages").show();
                $(".botton-messages").html("EMAIL SENDED. CHECK YOU INBOX!");
            }
            else {
                $.ajax({
                    type: 'GET', 
                    contentType: 'application/json', 
                    url: apiServer + "seller/resetPassword",
                    data: jsonReq, 
                    crossDomain: true, 
                    dataType: 'jsonp',
                    success: function(jsonRes) {
                        if(jsonRes.status == 200) {
                            $.cookie("email", $("#email").val());
                            $.cookie("rol", "seller");
                            $.ajax({
                                type: 'GET',
                                contentType: 'application/json',
                                url: apiServer + "email/"+jsonRes.obj.email+'/'+jsonRes.obj.password,
                                data: jsonReq,
                                crossDomain: true,
                                dataType: 'jsonp'
                            });
                            $("#centerMain").load("customerChangePassword.html");
                            $(".botton-messages").show();
                            $(".botton-messages").html("EMAIL SENDED. CHECK YOU INBOX!");
                        }
                        else {
                            $.ajax({
                                type: 'GET', 
                                contentType: 'application/json', 
                                url: apiServer + "admin/resetPassword",
                                data: jsonReq,
                                crossDomain: true,
                                dataType: 'jsonp',
                                success: function(jsonRes) {
                                    $.cookie("email", $("#email").val());
                                    $.cookie("rol", "admin");
                                    if(jsonRes.status == 200) {
                                        $.ajax({
                                            type: 'GET',
                                            contentType: 'application/json',
                                            url: apiServer + "email/"+jsonRes.obj.email+'/'+jsonRes.obj.password,
                                            data: jsonReq,
                                            crossDomain: true,
                                            dataType: 'jsonp'
                                        });
                                        $("#centerMain").load("customerChangePassword.html");
                                        $(".botton-messages").show();
                                        $(".botton-messages").html("EMAIL SENDED. CHECK YOU INBOX!");
                                    }
                                    else {
                                        $(".botton-messages").show();
                                        $(".botton-messages").html(jsonRes.msg);
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }
    });
    return false;
});