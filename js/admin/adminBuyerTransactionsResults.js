function confirm(){
    var jsonReq = {
        "_id": $.cookie("transactionId"),
        "statusInvoiceBuying": "PAID TO THE SELLER"
    };
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: apiServer + "transaction/updateStatusInvoiceBuying",
        data: jsonReq,
        crossDomain: true,
        dataType: 'jsonp',
        success: function(jsonRes) {
            if (jsonRes.status == 200) {
            } else {
                $(".botton-messages").show();
                $(".botton-messages").html(jsonRes.msg);
            }
            $("#results").load("views/admin/adminBuyerTransactionsResults.html");
        }
    });
}
function pay(){
    var jsonReq = {
        "_id": $.cookie("transactionId"),
        "statusInvoiceBuying": "PAID"
    };
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: apiServer + "transaction/statusInvoicePayment",
        data: jsonReq,
        crossDomain: true,
        dataType: 'jsonp',
        success: function(jsonRes) {
            if (jsonRes.status == 200) {
            } else {
                $(".botton-messages").show();
                $(".botton-messages").html(jsonRes.msg);
            }
            $("#results").load("views/admin/adminBuyerTransactionsResults.html");
        }
    });
}
$(document).ready(function () {
    // Transactions
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: apiServer + "transaction/readAll",
        //data: jsonReq,
        crossDomain: true,
        dataType: 'jsonp',
        success: function(jsonRes) {
            if (jsonRes.status == 200) {
                var data = [];
                for (var i = 0, l = jsonRes.obj.length; i < l; i++) {
                    var jsonItem = jsonRes.obj[i];
                    var item = {};
                    item.id = jsonItem._id;
                    item.transactionClosingDate = "NA"; // TODO: real data not NA
                    item.seller = "NA";
                    item.buyer = "NA";
                    item.totalAmount = "NA";
                    item.bestAdvance = "NA";
                    item.bestDiscount = "NA";
                    item.daysMaturity = "NA";
                    item.purchaseStatus =
                        jsonItem.statusInvoiceBuying == "PENDING OF CONFIRMATION"?
                            "PENDING OF CONFIRMATION <input type='submit' class=\"btn-blue\" onclick='confirm();' value='Confirm'/>" :
                            jsonItem.statusInvoiceBuying;
                    item.paymentStatus =
                        jsonItem.statusInvoicePayment == "PENDING OF PAYMENT"?
                            "PENDING OF PAYMENT <input type='submit' class=\"btn-blue\" onclick='pay();' value='Validate Payment'/>" :
                            jsonItem.statusInvoicePayment;
                    data.push(item);
                }
                $('#transactions').bootstrapTable({
                    data: data
                });
                $('#transactions').bootstrapTable('hideLoading');
                // Color encabezado
                $("#transactions thead tr th").addClass("bold header-profile-table");
            } else {
                $(".botton-messages").show();
                $(".botton-messages").html(jsonRes.msg);
            }
        }
    });
    $('#transactions').on('click-cell.bs.table', function(e, value, row, element){
        $.cookie("transactionId", element.id);
    });
});
