$(document).ready(function () {
    $("#frmBuyerEnrollment").on("submit", function(){
        var jsonReq = {
            name: $("#name").val(),
            personType: false,
            personName: $("#companyName").val(),
            legalAddress: $("#legalAddress").val(),
            phone: $("#phone").val(),
            email: $("#email").val(),
            bankAccount: {
                routingNumber: $("#routingNumber2").val(),
                accountNumber: $("#accountNumber2").val(),
                bankInfoNotes: $("#bankInfoNotes2").val()
            }
        };
        $.ajax({
            type: 'GET',
            contentType: 'application/json',
            url: apiServer + "buyer/create",
            data: jsonReq,
            crossDomain: true,
            dataType: 'jsonp',
            success: function(jsonRes) {
                if(jsonRes.status == 200) {
                    $("#centerMain").load("views/admin/adminBuyers.html");
                    $.ajax({
                        type: 'GET',
                        contentType: 'application/json',
                        url: apiServer + "email/"+jsonRes.obj.email+'/'+jsonRes.obj.password,
                        data: jsonReq,
                        crossDomain: true,
                        dataType: 'jsonp'
                    });
                }
                else {
                    $(".botton-messages").show();
                    $(".botton-messages").html(jsonRes.msg);
                }
            }
        });
        return false;
    });
});