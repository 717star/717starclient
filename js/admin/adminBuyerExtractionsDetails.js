$(document).ready(function () {
    var jsonReq = {
        "_id": $.cookie("extractionId")
    }
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: apiServer + "buyerMovement/readById",
        data: jsonReq,
        crossDomain: true,
        dataType: 'jsonp',
        success: function(jsonRes) {
            if (jsonRes.status == 200) {
                $("#name").html("N/A"); // TODO: real data
                $("#companyName").html("N/A");
                $("#legalAddress").html("N/A");
                $("#phone").html("N/A");
                $("#email").html("N/A");
                $("#routingNumber").html("N/A");
                $("#accountNumber").html("N/A");
                $("#ammount").html(jsonRes.obj.amount);
            } else {
                $(".botton-messages").show();
                $(".botton-messages").html(jsonRes.msg);
            }
        }
    });
    $("#modalExtractionsDetails").modal("show");
    $("#confirm").click(function(){
        $("#centerMain").load("views/admin/adminBuyerExtractionsConfirm.html");
    });
    $("#cancel").click(function(){
        $("#centerMain").load("views/admin/adminBuyerExtractions.html");
    });
});