function details(){
    $("#modal").load("views/admin/adminSellerDetails.html");
}

function invoices(){
    $("#centerMain").load("views/admin/adminInvoices.html");
}

function transactions(){
    $("#centerMain").load("views/admin/adminBuyerTransactions.html");
}

$(document).ready(function () {
    var jsonReq = {
        "industry": $.cookie("industry"),
        "country": $.cookie("country")
    };
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: apiServer + "seller/readAllByIndustryCountry",
        data: jsonReq,
        crossDomain: true,
        dataType: 'jsonp', // TODO: all?
        success: function(jsonRes) {
            if (jsonRes.status == 200) {
                var data = [];
                for (var i = 0, l = jsonRes.obj.length; i < l; i++) {
                    var jsonItem = jsonRes.obj[i];
                    var item = {};
                    item.id = jsonItem._id;
                    item.name = jsonItem.companyName;
                    item.email = jsonItem.email;
                    item.country = jsonItem.country;
                    item.industry = jsonItem.activityIndustry;
                    item.actions = "<button class=\"btn-gray\" value=\"Details\" onclick='details();'>Details</button><button class=\"btn-gray\" value=\"Invoices\" onclick='invoices();'>Invoices</button><button class=\"btn-gray\" value=\"Transactions\" onclick='transactions();'>Transactions</button>"
                    data.push(item);
                }
                $('#sellers').bootstrapTable({
                    data: data
                });
                $('#sellers').bootstrapTable('hideLoading');
                // Color encabezado
                $("#sellers thead tr th").addClass("bold header-buyers-table");
            } else {
                $(".botton-messages").show();
                $(".botton-messages").html(jsonRes.msg);
            }
        }
    });
    $('#sellers').on('click-cell.bs.table', function(e, value, row, element){
        $.cookie("sellerId", element.id);
        $.cookie("sellerName", element.name);
    });
});