$(document).ready(function () {
    $("#buyer").focus();
    var buyerId = $.cookie("buyerId");
    if(buyerId) {
        var jsonReq = {
            "_id": buyerId
        };
        $.ajax({
            type: 'GET',
            contentType: 'application/json',
            url: apiServer + "buyer/readById",
            data: jsonReq,
            crossDomain: true,
            dataType: 'jsonp',
            success: function(jsonRes) {
                if (jsonRes.status == 200) {
                    $("#buyer").val(jsonRes.obj.name);
                    $("#routingNumber").val(jsonRes.obj.bankAccount.routingNumber);
                    $("#accountNumber").val(jsonRes.obj.bankAccount.accountNumber);
                } else {
                    $(".botton-messages").show();
                    $(".botton-messages").html(jsonRes.msg);
                }
            }
        });
    }
    $("#buyer").focusout(function() {
        $(".botton-messages").hide();
        var jsonReq = {
            "name": $("#buyer").val()
        };
        $.ajax({
            type: 'GET',
            contentType: 'application/json',
            url: apiServer + "buyer/readByName",
            data: jsonReq,
            crossDomain: true,
            dataType: 'jsonp',
            success: function (jsonRes) {
                if (jsonRes.status == 200) {
                    $("#buyer").val(jsonRes.obj.name);
                    $("#routingNumber").val(jsonRes.obj.bankAccount.routingNumber);
                    $("#accountNumber").val(jsonRes.obj.bankAccount.accountNumber);
                } else {
                    $("#buyer").focus();
                    $(".botton-messages").show();
                    $(".botton-messages").html(jsonRes.msg);
                }
            }
        });
    });
    $("#frmBuyerDeposit").on("submit", function() {
        $(".botton-messages").hide();
        var jsonReq = {
            "name": $("#buyer").val()
        };
        $.ajax({
            type: 'GET',
            contentType: 'application/json',
            url: apiServer + "buyer/readByName",
            data: jsonReq,
            crossDomain: true,
            dataType: 'jsonp',
            success: function(jsonRes) {
                if (jsonRes.status == 200) {
                    buyerId = jsonRes.obj._id;
                    jsonReq = {
                        buyer: buyerId,
                        amount: $("#ammount").val(),
                        movementType: "DEPOSIT",
                    };
                    $.ajax({
                        type: 'GET',
                        contentType: 'application/json',
                        url: apiServer + "buyerMovement/create",
                        data: jsonReq,
                        crossDomain: true,
                        dataType: 'jsonp',
                        success: function (jsonRes) {
                            if (jsonRes.status == 200) {
                                $("#ammount").val("");
                                $("#ammount").focus();
                            } else {
                                $(".botton-messages").show();
                                $(".botton-messages").html(jsonRes.msg);
                            }
                        }
                    });
                } else {
                    $(".botton-messages").show();
                    $(".botton-messages").html(jsonRes.msg);
                }
            }
        });
        return false;
    });
});