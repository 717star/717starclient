$(document).ready(function () {
    var sellerName = $.cookie("sellerName");
    if(sellerName) {
        $("#seller").val(sellerName);
    }
    $("#search").on("click", function () {
        $.cookie("seller", $("#seller").val());
        $.cookie("buyer", $("#buyer").val());
        $.cookie("invoiceNumber", $("#invoiceNumber").val());
        $("#results").load("views/admin/adminInvoicesResults.html");
    });
});
