$(document).ready(function () {

    // data - TODO put real data
    var dataSeller=
        [
            {
                "seller": "Marco Aldaroso <em>Mexico</em>",
                "issuing-date": "08/09/2016",
                "days-maturity": "3",
                "amount": "U$S 40.000,00",
                "ask-advance": "15%",
                "ask-discount": "15%"
            }
        ];

    var dataBuyer=
        [
            {
                "buyer": "John V. Wellington",
                "best-advance": "<em class='color-red bold'>Adv 90% Disc 2%</em>",
                "best-discount": "<em class='color-red bold'>Adv 85% Disc 1%</em>"
            },
            {
                "buyer": "John V. Wellington",
                "best-advance": "Adv 90% Disc 2%",
                "best-discount": "Adv 85% Disc 1%"
            },
            {
                "buyer": "John V. Wellington",
                "best-advance": "Adv 90% Disc 2%",
                "best-discount": "Adv 85% Disc 1%"
            },
            {
                "buyer": "John V. Wellington",
                "best-advance": "Adv 90% Disc 2%",
                "best-discount": "Adv 85% Disc 1%"
            },
            {
                "buyer": "John V. Wellington",
                "best-advance": "Adv 90% Disc 2%",
                "best-discount": "Adv 85% Disc 1%"
            },
            {
                "buyer": "John V. Wellington",
                "best-advance": "Adv 90% Disc 2%",
                "best-discount": "Adv 85% Disc 1%"
            },
            {
                "buyer": "John V. Wellington",
                "best-advance": "Adv 90% Disc 2%",
                "best-discount": "Adv 85% Disc 1%"
            },
            {
                "buyer": "John V. Wellington",
                "best-advance": "Adv 90% Disc 2%",
                "best-discount": "Adv 85% Disc 1%"
            },
            {
                "buyer": "John V. Wellington",
                "best-advance": "Adv 90% Disc 2%",
                "best-discount": "Adv 85% Disc 1%"
            },
            {
                "buyer": "John V. Wellington",
                "best-advance": "Adv 90% Disc 2%",
                "best-discount": "Adv 85% Disc 1%"
            }
        ];

    $('#sellers').bootstrapTable({
        data: dataSeller
    });

    $('#sellers').bootstrapTable('hideLoading');

    // Color encabezado
    $("#sellers thead tr th").addClass("bold header-profile-table");

    $('#buyers').bootstrapTable({
        data: dataBuyer
    });

    $('#buyers').bootstrapTable('hideLoading');

    // Color encabezado
    $("#buyers thead tr th").addClass("bold header-profile-table");

    $("#modalBids").modal("show");
});
