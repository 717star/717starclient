$(document).ready(function () {
    var jsonReq = {
        "_id": $.cookie("sellerId")
    };
    $.ajax({
        type: 'GET', 
        contentType: 'application/json', 
        url: apiServer + "seller/readById", 
        data: jsonReq, 
        crossDomain: true, 
        dataType: 'jsonp',
        success: function(jsonRes) {
            if (jsonRes.status == 200) {
                $("#companyName").html(jsonRes.obj.companyName);
                $("#activityIndustry").html(jsonRes.obj.activityIndustry);
                $("#countryC").html(jsonRes.obj.country);
                $("#contactFirstLastName").html(jsonRes.obj.contactFirstLastName);
                $("#phone").html(jsonRes.obj.phone);
                $("#email").html(jsonRes.obj.email);
                $("#legalAddress").html(jsonRes.obj.legalAddress);
                $("#webSite").html(jsonRes.obj.webSite);
                $("#routingNumber").html(jsonRes.obj.bankAccount.routingNumber);
                $("#swiftNumber").html(jsonRes.obj.bankAccount.swiftNumber);
                $("#accountNumber").html(jsonRes.obj.bankAccount.accountNumber);
                $("#bankInfoNotes").html(jsonRes.obj.bankAccount.bankInfoNotes);
                $("#modalSellerDetails").modal("show");
            } else {
                $(".botton-messages").show();
                $(".botton-messages").html(jsonRes.msg);
            }
        }
    });
});