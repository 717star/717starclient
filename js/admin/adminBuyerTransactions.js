$(document).ready(function () {
    var sellerName = $.cookie("sellerName");
    var buyerName = $.cookie("buyerName");
    if(sellerName) {
        $("#seller").val(sellerName);
    }
    if(buyerName) {
        $("#buyer").val(buyerName);
    }
    $("#search").on("click", function () {
        $.cookie("sellerName", $("#seller").val());
        $.cookie("buyerName", $("#buyer").val());
        $("#results").load("views/admin/adminBuyerTransactionsResults.html");
    });
});
