function extractionConfirm(extractionId) {
    $("#modal").load("views/admin/adminBuyerExtractionsDetails.html");
}

function extractionCancel(extractionId) {
    $("#modal").load("views/admin/adminBuyerExtractionsDecline.html");
}

$(document).ready(function () {
    var jsonReq = {
        "movementType": "WITHDRAWAL",
        "status": "PENDING VALIDATION"
    };
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: apiServer + "buyerMovement/readAllByTypeAndStatus",
        data: jsonReq,
        crossDomain: true,
        dataType: 'jsonp',
        success: function(jsonRes) {
            if (jsonRes.status == 200) {
                var data = [];
                for (var i = 0, l = jsonRes.obj.length; i < l; i++) {
                    var jsonItem = jsonRes.obj[i];
                    var item = {};
                    item.id = jsonItem._id;
                    item.name = jsonItem.buyer.name;
                    item.personName = jsonItem.buyer.personName;
                    item.legalAddress = jsonItem.buyer.legalAddress;
                    item.phone = jsonItem.buyer.phone;
                    item.email = jsonItem.buyer.email;
                    item.routingNumber = jsonItem.buyer.bankAccount.routingNumber;
                    item.accountNumber = jsonItem.buyer.bankAccount.accountNumber;
                    item.amount = jsonItem.amount;
                    item.actions = "<button class=\"btn-green\" value=\"Accept\" onclick='extractionConfirm();'>Accept</button><button class=\"btn-dialog-cancel decline\" value=\"Decline\" onclick='extractionCancel();'>Decline</button>"
                    data.push(item);
                }
                $('#extractions').bootstrapTable({
                    data: data
                });
                $('#extractions').bootstrapTable('hideLoading');
                // Color encabezado
                $("#extractions thead tr th").addClass("bold header-buyers-table");
            } else {
                $(".botton-messages").show();
                $(".botton-messages").html(jsonRes.msg);
            }
        }
    });
    $('#extractions').on('click-cell.bs.table', function(e, value, row, element){
        $.cookie("extractionId", element.id);
    });
});