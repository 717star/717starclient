$(document).ready(function () {
    $("#frmSellerEnrollment").on("submit", function(){
        var jsonReq = {
            companyName: $("#companyName").val(),
            activityIndustry: $("#activityIndustry").val(),
            country: $("#country").val(),
            contactFirstLastName: $("#contactFirstLastName").val(),
            phone: $("#phone").val(),
            email: $("#email").val(),
            legalAddress: $("#legalAddress").val(),
            webSite: $("#webSite").val(),
            bankAccount: {
                routingNumber: $("#routingNumber").val(),
                swiftNumber: $("#swiftNumber").val(),
                accountNumber: $("#accountNumber").val(),
                bankInfoNotes: $("#bankInfoNotes").val()
            }
        };
        $.ajax({
            type: 'GET',
            contentType: 'application/json',
            url: apiServer + "seller/create",
            data: jsonReq,
            crossDomain: true,
            dataType: 'jsonp',
            success: function(jsonRes) {
                if(jsonRes.status == 200) {
                    $("#centerMain").load("views/admin/adminSellers.html");
                    $.ajax({
                        type: 'GET',
                        contentType: 'application/json',
                        url: apiServer + "email/"+jsonRes.obj.email+'/'+jsonRes.obj.password,
                        data: jsonReq,
                        crossDomain: true,
                        dataType: 'jsonp'
                    });
                }
                else {
                    $(".botton-messages").show();
                    $(".botton-messages").html(jsonRes.msg);
                }
            }
        });
        return false;
    });
});