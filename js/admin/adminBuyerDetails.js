$(document).ready(function () {
    var jsonReq = {
        "_id": $.cookie("buyerId")
    };
    $.ajax({
        type: 'GET', 
        contentType: 'application/json', 
        url: apiServer + "buyer/readById", 
        data: jsonReq, 
        crossDomain: true, 
        dataType: 'jsonp',
        success: function(jsonRes) {
            if (jsonRes.status == 200) {
                $("#name").html(jsonRes.obj.name);
                $("#companyName").html(jsonRes.obj.personName);
                $("#legalAddress").html(jsonRes.obj.legalAddress);
                $("#phone").html(jsonRes.obj.phone);
                $("#email").html(jsonRes.obj.email);
                $("#routingNumber").html(jsonRes.obj.bankAccount.routingNumber);
                $("#accountNumber").html(jsonRes.obj.bankAccount.accountNumber);
                $("#bankInfoNotes").html(jsonRes.obj.bankAccount.bankInfoNotes);
                $("#modalBuyerDetails").modal("show");
            } else {
                $(".botton-messages").show();
                $(".botton-messages").html(jsonRes.msg);
            }
        }
    });
});