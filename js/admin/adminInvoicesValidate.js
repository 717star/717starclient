$(document).ready(function () {
    var jsonReq = {
        "_id": $.cookie("invoiceId")
    };
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: apiServer + "invoice/readById",
        data: jsonReq,
        crossDomain: true,
        dataType: 'jsonp',
        success: function(jsonRes) {
            if (jsonRes.status == 200) {
                $("#companyName").html(jsonRes.obj.payerImporter.companyName);
                $("#country").html(jsonRes.obj.payerImporter.country);
                $("#person").html(jsonRes.obj.payerImporter.contact.person);
                $("#email").html(jsonRes.obj.payerImporter.contact.email);
                $("#phone").html(jsonRes.obj.payerImporter.contact.phone);
                $("#cel").html(jsonRes.obj.payerImporter.contact.cel? jsonRes.obj.payerImporter.contact.cel : "-");
                $("#invoiceNumber2").html(jsonRes.obj.invoiseId);
                $("#totalAmount").html(jsonRes.obj.totalAmount);
                // $("#purchaseDate").html(""); TODO: not set on create
                $("#issueDate").html(jsonRes.obj.issueDate);
                $("#expirationDate").html(jsonRes.obj.expirationDate);
                $("#sent").html(jsonRes.obj.sent? "Yes" : "No");
                $("#billOfLadingDate").html(jsonRes.obj.billOfLadingDate? jsonRes.obj.billOfLadingDate : "-");
                $("#incoterms").html(jsonRes.obj.incoterms? jsonRes.obj.incoterms : "-");
                $("#authorizedByDebtor").html(jsonRes.obj.authorizedByDebtor? "Yes" : "No");
                $("#askAdvance").html(jsonRes.obj.askAdvance);
                $("#askDiscountRate").html(jsonRes.obj.askDiscountRate);
            } else {
                $(".botton-messages").show();
                $(".botton-messages").html(jsonRes.msg);
            }
            $("#modalInvoicesValidate").modal("show");
        }
    });
    $("#validate").click(function(){
        $("#modal").load("views/admin/adminInvoicesValidateConfirm.html");
    });
    $("#invalidate").click(function(){
        $("#modal").load("views/admin/adminInvoicesInvalidateConfirm.html");
    });
    $("#cancel").click(function(){
        $("#centerMain").load("views/admin/adminInvoices.html");
    });
});