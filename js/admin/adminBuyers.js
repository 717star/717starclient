function details(){
    $("#modal").load("views/admin/adminBuyerDetails.html");
}

function deposits(){
    $("#centerMain").load("views/admin/adminBuyerDeposits.html");
}

function transactions(){
    $("#centerMain").load("views/admin/adminBuyerTransactions.html");
}

$(document).ready(function () {
    $.ajax({
        type: 'GET', 
        contentType: 'application/json', 
        url: apiServer + "buyer/readAll", 
        crossDomain: true, 
        dataType: 'jsonp',
        success: function(jsonRes) {
            if (jsonRes.status == 200) {
                var data = [];
                for (var i = 0, l = jsonRes.obj.length; i < l; i++) {
                    var jsonItem = jsonRes.obj[i];
                    var item = {};
                    item.id = jsonItem._id;
                    item.name = jsonItem.name;
                    item.email = jsonItem.email;
                    item.actions = "<button class=\"btn-gray\" value=\"Details\" onclick='details();'>Details</button><button class=\"btn-gray\" value=\"Deposit\" onclick='deposits();'>Deposit</button><button class=\"btn-gray\" value=\"Transactions\" onclick='transactions();'>Transactions</button>"
                    data.push(item);
                }
                $('#buyers').bootstrapTable({
                    data: data
                });
                $('#buyers').bootstrapTable('hideLoading');
                // Color encabezado
                $("#buyers thead tr th").addClass("bold header-buyers-table");
            } else {
                $(".botton-messages").show();
                $(".botton-messages").html(jsonRes.msg);
            }
        }
    });
    $('#buyers').on('click-cell.bs.table', function(e, value, row, element){
        $.cookie("buyerId", element.id);
        $.cookie("buyerName", element.name);
    });
});