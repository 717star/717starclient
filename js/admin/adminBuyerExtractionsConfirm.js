$(document).ready(function () {
    var extractionId = $.cookie("extractionId");
    $("#confirm").click(function(){
        var jsonReq = {
            "_id": $.cookie("extractionId")
        }
        $.ajax({
            type: 'GET',
            contentType: 'application/json',
            url: apiServer + "buyerMovement/activate",
            data: jsonReq,
            crossDomain: true,
            dataType: 'jsonp',
            success: function(jsonRes) {
                if (jsonRes.status == 200) {
                } else {
                    $(".botton-messages").show();
                    $(".botton-messages").html(jsonRes.msg);
                }
            }
        });
        $("#centerMain").load("views/admin/adminBuyerExtractions.html");
    });
    $("#cancel").click(function(){
        $("#centerMain").load("views/admin/adminBuyerExtractionsDetails.html");
    });
    $("#modalApproveConfirm").modal("show");
});