// Cells with states
function pintarEstados(value){
    var classes = ["state-gray", "state-green", "state-blue"];

    if(value == "IN BIDDING")
    {
        return {classes: "state-green"}
    }

    if(value == "PENDING VALIDATION")
    {
        return {classes: "state-gray"}
    }
    return {classes: "state-blue"}
}

function validate(){
    $("#modal").load("views/admin/adminInvoicesValidate.html")
}

function viewBids(){
    $("#modal").load("views/admin/adminViewBids.html")
}

$(document).ready(function () {
    var seller = $.cookie("seller");
    var buyer = $.cookie("buyer");
    var invoiceNumber = $.cookie("invoiceNumber");
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: apiServer + "invoice/readAllToValidate",
        //data: jsonReq,
        crossDomain: true,
        dataType: 'jsonp',
        success: function(jsonRes) {
            if (jsonRes.status == 200) {
                var data = [];
                for (var i = 0, l = jsonRes.obj.length; i < l; i++) {
                    var jsonItem = jsonRes.obj[i];
                    var item = {};
                    item.id = jsonItem._id; // TODO: real data not NA
                    item.seller = "NA";
                    item.buyer = "NA";
                    item.issueDate = jsonItem.issueDate;
                    item.daysMaturity = "NA";
                    item.totalAmount = jsonItem.totalAmount;
                    item.askAdvance = jsonItem.askAdvance;
                    item.askDiscountRate = jsonItem.askDiscountRate;
                    item.bestAdvance = "NA";
                    item.bestDiscount = "NA";
                    item.status = jsonItem.status;
                    item.biddingClosing = "NA";
                    item.actions = jsonItem.status = "PENDING VALIDATION"?
                        "<button class=\"btn-green\" onclick='validate();'>Validate</button>" :
                        "<button class=\"btn-blue\" onclick='viewBids();'View BIDs</button>";
                    data.push(item);
                }
                $('#invoices').bootstrapTable({
                    data: data
                });
                $('#invoices').bootstrapTable('hideLoading');
                // Color encabezado
                $("#invoices thead tr th").addClass("bold header-profile-table");
            } else {
                $(".botton-messages").show();
                $(".botton-messages").html(jsonRes.msg);
            }
        }
    });
    $('#invoices').on('click-cell.bs.table', function(e, value, row, element){
        $.cookie("invoiceId", element.id);
    });
});
