$(document).ready(function () {
    $("#confirm").click(function(){
        var jsonReq = {
            "_id": $.cookie("extractionId")
        }
        $.ajax({
            type: 'GET',
            contentType: 'application/json',
            url: apiServer + "buyerMovement/desactivate",
            data: jsonReq,
            crossDomain: true,
            dataType: 'jsonp',
            success: function(jsonRes) {
                if (jsonRes.status == 200) {
                } else {
                    $(".botton-messages").show();
                    $(".botton-messages").html(jsonRes.msg);
                }
            }
        });
        $("#centerMain").load("views/admin/adminBuyerExtractions.html");
    });
    $("#cancel").click(function(){
        $("#centerMain").load("views/admin/adminBuyerExtractionsDecline.html");
    });
    $("#modalDeclineConfirm").modal("show");
});