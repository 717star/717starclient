$(document).ready(function () {
    $("#confirm").click(function(){
        var jsonReq = {
            "_id": $.cookie("invoiceId")
        };
        $.ajax({
            type: 'GET',
            contentType: 'application/json',
            url: apiServer + "invoice/validate",
            data: jsonReq,
            crossDomain: true,
            dataType: 'jsonp',
            success: function(jsonRes) {
                if (jsonRes.status == 200) {
                } else {
                    $(".botton-messages").show();
                    $(".botton-messages").html(jsonRes.msg);
                }
            }
        });
        $("#centerMain").load("views/admin/adminInvoices.html");
    });
    $("#cancel").click(function(){
        $("#centerMain").load("views/admin/adminInvoicesValidate.html");
    });
    $("#modalValidateConfirm").modal("show");
});