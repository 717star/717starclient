$("#frmCustomerLogon").on("submit", function () {
    var jsonReq = {
        email: $("#email").val(),
        password: $("#password").val()
    };
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: apiServer + "buyer/readByEmailPassword",
        data: jsonReq,
        crossDomain: true,
        dataType: 'jsonp',
        success: function(jsonRes) {
            if(jsonRes.status == 200) {
                $.cookie("userId", jsonRes.obj._id);
                $.cookie("rol", "buyer");
                $.cookie("authorized", true);
                window.location.href = 'profilesLayout.html';
            }
            else {
                $.ajax({
                    type: 'GET',
                    contentType: 'application/json',
                    url: apiServer + "seller/readByEmailPassword",
                    data: jsonReq,
                    crossDomain: true,
                    dataType: 'jsonp',
                    success: function(jsonRes) {
                        if(jsonRes.status == 200) {
                            $.cookie("userId", jsonRes.obj._id);
                            $.cookie("rol", "seller");
                            $.cookie("authorized", true);
                            window.location.href = 'profilesLayout.html';
                        }
                        else {
                            $.ajax({
                                type: 'GET',
                                contentType: 'application/json',
                                url: apiServer + "admin/readByEmailPassword",
                                data: jsonReq,
                                crossDomain: true,
                                dataType: 'jsonp',
                                success: function(jsonRes) {
                                    if(jsonRes.status == 200) {
                                        $.cookie("userId", jsonRes.obj._id);
                                        $.cookie("rol", "admin");
                                        $.cookie("authorized", true);
                                        window.location.href = 'profilesLayout.html';
                                    }
                                    else {
                                        $(".botton-messages").show();
                                        $(".botton-messages").html(jsonRes.msg);
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }
    });
    return false;
});