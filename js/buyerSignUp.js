$("#frmSignUp").on("submit", function () {
    if ($("#terms").is(':checked')) {
        var jsonReq = {
            name: $("#name").val(),
            personType: $("#individual").val(),
            personName: $("#nameCompany").val(),
            legalAddress: $("#legalAddress").val(),
            phone: $("#phone").val(),
            email: $("#email").val(),
            bankAccount: {
                routingNumber: "NA",
                accountNumber: "NA",
                bankInfoNotes: "NA"
            }
        };
        $.ajax({
            type: 'GET',
            contentType: 'application/json',
            url: apiServer + "buyer/create",
            data: jsonReq,
            crossDomain: true,
            dataType: 'jsonp',
            success: function (jsonRes) {
                if(jsonRes.status == 200) {
                    $.cookie("email", $("#email").val());
                    $.cookie("rol", "buyer");
                    $("#centerMain").load("customerChangePassword.html");
                    $(".botton-messages").show();
                    $(".botton-messages").html("EMAIL SENDED. CHECK YOU INBOX!");
                    $.ajax({
                        type: 'GET',
                        contentType: 'application/json',
                        url: apiServer + "email/"+jsonRes.obj.email+'/'+jsonRes.obj.password,
                        data: jsonReq,
                        crossDomain: true,
                        dataType: 'jsonp'
                    });
                } else {
                    $(".botton-messages").show();
                    $(".botton-messages").html(jsonRes.msg);
                }
            }
        });
    }
    else{
        $(".botton-messages").show();
        $(".botton-messages").html("YOU MUST ACCEPT TERMS AND CONDITIONS!");
    }
    return false;
});